<?php
namespace app\admin\controller;

use think\Request;
use app\common\model\Config as ConfigModel;

class Theme extends Admin
{

    public function index()
    {
        // 查询模版列表
        $list = $this->getTheme();

        // 查询已经启用模版
        $theme = ConfigModel::getValue('theme');
        
        $this->assign('list', $list);
        $this->assign('theme', $theme);
        return $this->fetch('admin/theme/index');
    }

    public function change()
    {
        $themeName = Request::instance()->param('name');
        if (!empty($themeName)) {
            ConfigModel::setValue('theme', $themeName);
        } else {
            return $this->response(202);
        }
        
        return $this->response(200);
    }

}
