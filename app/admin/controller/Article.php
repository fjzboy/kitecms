<?php
namespace app\admin\controller;

use app\admin\controller\Admin;
use think\Request;
use app\common\model\Article as ArticleModel;
use app\common\logic\Article as ArticleLogic;
use app\common\logic\Category as CategoryLogic;
use app\common\logic\Tag as TagLogic;
use app\common\model\Modoule;

class Article extends Admin
{
    private $categoryLogic;

    private $articleLogic;

    function _initialize()
    {
        $this->categoryLogic = new CategoryLogic();
        $this->articleLogic = new ArticleLogic();
    }

    public function index()
    {
        $search = Request::instance()->param();

        $map = array();
        //搜索条件
        if (isset($search['title']) && $search['title'] != '') {
            $map['a.title'] = array('like', '%'.$search['title'].'%');
        }
        if (isset($search['category_id']) && $search['category_id'] != '') {
            $map['category_id'] = $search['category_id'];
        }

        //文章列表
        $list = ArticleModel::getList($map, 'id desc', 15, $search);
        
        $page = $list->render();
        //分类列表 
        $cate = $this->categoryLogic->getList();
        $this->assign('search', $search);
        $this->assign('cate', $cate);
        $this->assign('list', $list);
        $this->assign('page', $page);
        return $this->fetch('admin/article/index');
    }

    public function handle()
    {
        $data = Request::instance()->param();
        // print_r($data);
        switch ($data['type']) {
            case 'delete':
                foreach ($data['ids'] as $v) {
                    $result = ArticleModel::remove($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'recommend':
                foreach ($data['ids'] as $v) {
                    $result = ArticleModel::recommend($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'hot':
                foreach ($data['ids'] as $v) {
                    $result = ArticleModel::hot($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'cancelrecommend':
                foreach ($data['ids'] as $v) {
                    $result = ArticleModel::recommend($v, 0);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'cancelhot':
                foreach ($data['ids'] as $v) {
                    $result = ArticleModel::hot($v, 0);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'change':
                foreach ($data['ids'] as $v) {
                    $result = ArticleModel::setStatus($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
        }
        return $this->response(201);
    }

    public function create()
    {
        $category = $this->categoryLogic->getList();

        $new_category = [];
        if (!empty($category)) {
            foreach ($category as $v) {
                // 禁用
                if ($v['type'] == 1 || $v['table_name'] != 'article'){
                    $v['disabled'] = 'disabled';
                } else {
                    $v['disabled'] = '';
                }
                array_push($new_category, $v);
            }
        }

        $this->assign('category', $new_category);
        return $this->fetch('admin/article/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        $data['uid'] = $this->uid;

        // 检测分类信息 是否允许发布文章
        $category = \app\common\model\Category::getInfo($data['category_id']);
        if ($category['type'] != 0) {
            return $this->response(1301);
        }

        // 模型不匹配 不允许发布文章
        if ($category['table_name'] != 'article') {
            return $this->response(1302);
        }

        $ret = $this->articleLogic->add($data);

        if (is_numeric($ret)) {
            // 增加TAG
            $tagLogic = new TagLogic();
            $tag = $tagLogic->add($data['keywords'], $ret);
            return $this->response(200);
        } else {
            return $this->response(201,$ret);
        }

    }

    public function edit()
    {
        $id = Request::instance()->param('id');
        $article = ArticleModel::getInfo($id);

        //处理封面图片路径
        if (!empty($article['image'])) {
            $article['image'] = buildImageUrl($article['image']);
        }

        $category = $this->categoryLogic->getList();
        $new_category = [];
        if (!empty($category)) {
            foreach ($category as $v) {
                // 禁用
                if ($v['type'] == 1 || $v['table_name'] != 'article'){
                    $v['disabled'] = 'disabled';
                } else {
                    $v['disabled'] = '';
                }
                // 选中
                if ($article['category_id'] == $v['id']) {
                    $v['selected'] = 'selected';
                } else {
                    $v['selected'] = '';
                }
                array_push($new_category, $v);
            }
        }

        $this->assign('article', $article);
        $this->assign('category', $new_category);
        return $this->fetch('admin/article/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();

        // 检测分类信息 是否允许发布文章
        $category = \app\common\model\Category::getInfo($data['category_id']);
        if ($category['type'] != 0) {
            return $this->response(1301);
        }

        // 模型不匹配 不允许发布文章
        if ($category['table_name'] != 'article') {
            return $this->response(1302);
        }

        $tag = ArticleModel::getKeywords($data['id']);
        $ret = $this->articleLogic->modify($data);

        if (is_numeric($ret)) {
            // 更新tag关系
            $tagLogic = new TagLogic();
            $tagLogic->modify($data['keywords'], $data['id']);

            return $this->response(200);
        } else {
            return $this->response(201,$ret);
        }
    }

    public function remove()
    {
        $id = Request::instance()->param('id');
        $result = CategoryModel::remove($id);
        if ($result !== false) {
            return $this->response(200);
        } else {
            return $this->response(100);
        }
    }

}
