<?php
namespace app\admin\controller;

use think\Request;
use think\Db;
use app\admin\controller\Admin;

class Index extends Admin
{
    public function index()
    {
        // 报表数据信息
        $data = array(
            'article' => ['title' => '文章总数',
             'total' => Db::name('article')->count(),
             'newly' => Db::name('article')->where('create_at', '>', date('Y-m-d', time()))->count(),
             'pending' => Db::name('article')->where('status', 0)->count(),
            ],

            'book' => ['title' => '图书总数',
             'total' => Db::name('book')->count(),
             'newly' => Db::name('book')->where('create_at', '>', date('Y-m-d', time()))->count(),
             'pending' => Db::name('book')->where('status', 0)->count(),
            ],

            'comment' => ['title' => '评论总数',
             'total' => Db::name('article_comment')->count(),
             'newly' => Db::name('article_comment')->where('create_at', '>', date('Y-m-d', time()))->count(),
             'pending' => Db::name('article_comment')->where('status', 0)->count(),
            ],

            'member' => ['title' => '用户总数',
             'total' => Db::name('user')->count(),
             'newly' => Db::name('user')->where('create_at', '>', date('Y-m-d', time()))->count(),
             'pending' => Db::name('user')->where('status', 0)->count(),
            ]
        );

        foreach ($data as $k => $v) {
            $this->assign($k, $v);
        }

        //服务器信息
        $mysql = Db::query('select version() as ver');
        $server = array(
            'version' => 'v1.0',
            'os'      => PHP_OS,
            'php'     => $_SERVER['SERVER_SOFTWARE'],
            'mysql'   => $mysql[0]['ver'],
            'upload'  => ini_get("file_uploads") ? ini_get("upload_max_filesize") : "Disabled",
        );
        $this->assign('server', $server);

        return $this->fetch('admin/index/index');
    }
}
