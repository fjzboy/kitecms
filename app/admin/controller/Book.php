<?php
namespace app\admin\controller;

use think\Request;
use app\admin\controller\Admin;
use app\common\model\Book as BookModel;
use app\common\logic\Book as BookLogic;
use app\common\logic\Category as CategoryLogic;

class Book extends Admin
{
    private $categoryLogic;

    function _initialize()
    {
        $this->categoryLogic = new CategoryLogic();
    }

    public function index()
    {
        $search = Request::instance()->param();

        $map = array();
        // 搜索条件
        if (isset($search['title']) && $search['title'] != '') {
            $map['a.title'] = array('like', '%'.$search['title'].'%');
        }
        if (isset($search['category_id']) && $search['category_id'] != '') {
            $map['category_id'] = $search['category_id'];
        }

        // 图书列表
        $list = BookModel::getList($map, 'id desc', 15, $search);

        // 分类列表 
        $category = $this->categoryLogic->getList();

        $this->assign('search', $search);
        $this->assign('category', $category);
        $this->assign('list', $list);
        $this->assign('page', $list->render());
        return $this->fetch('admin/book/index');
    }

    public function handle()
    {
        $data = Request::instance()->param();
        // print_r($data);
        switch ($data['type']) {
            case 'delete':
                foreach ($data['ids'] as $v) {
                    $result = BookModel::remove($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'recommend':
                foreach ($data['ids'] as $v) {
                    $result = BookModel::recommend($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'hot':
                foreach ($data['ids'] as $v) {
                    $result = BookModel::hot($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'cancelrecommend':
                foreach ($data['ids'] as $v) {
                    $result = BookModel::recommend($v, 0);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'cancelhot':
                foreach ($data['ids'] as $v) {
                    $result = BookModel::hot($v, 0);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'change':
                foreach ($data['ids'] as $v) {
                    $result = BookModel::setStatus($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
        }
        return $this->response(201);
    }

    public function create()
    {
        $category = $this->categoryLogic->getList();

        // 处理分类 如果模式不是列表 和 绑定模型不是 book 则禁用
        $new_category = [];
        if (!empty($category)) {
            foreach ($category as $v) {
                // 禁用
                if ($v['type'] == 1 || $v['table_name'] != 'book'){
                    $v['disabled'] = 'disabled';
                } else {
                    $v['disabled'] = '';
                }
                array_push($new_category, $v);
            }
        }

        $this->assign('category',$new_category);
        return $this->fetch('admin/book/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        $data['uid'] = $this->uid;

        // 检测分类信息 是否允许发布文章
        $category = \app\common\model\Category::getInfo($data['category_id']);
        if ($category['type'] != 0) {
            return $this->response(1301);
        }

        // 模型不匹配 不允许发布文章
        if ($category['table_name'] != 'book') {
            return $this->response(1302);
        }

        $logic = new BookLogic();
        $ret = $logic->add($data);

        if (is_numeric($ret)) {
            return $this->response(200);
        } else {
            return $this->response(201,$ret);
        }

    }

    public function edit()
    {
        $book_id = Request::instance()->param('id');

        // 查询图书信息
        $book = BookModel::getInfo($book_id);

        // 处理封面图片路径
        if (!empty($book['image'])) {
            $book['image'] = buildImageUrl($book['image']);
        }

        $category = $this->categoryLogic->getList();

        // 处理分类 如果模式不是列表 和 绑定模型不是 book 则禁用
        $new_category = [];
        if (!empty($category)) {
            foreach ($category as $v) {
                // 禁用
                if ($v['type'] == 1 || $v['table_name'] != 'book'){
                    $v['disabled'] = 'disabled';
                } else {
                    $v['disabled'] = '';
                }
                // 选中
                if ($book['category_id'] == $v['id']) {
                    $v['selected'] = 'selected';
                } else {
                    $v['selected'] = '';
                }
                array_push($new_category, $v);
            }
        }

        $this->assign('book', $book);
        $this->assign('category', $new_category);
        return $this->fetch('admin/book/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();

        // 检测分类信息 是否允许发布文章
        $category = \app\common\model\Category::getInfo($data['category_id']);
        if ($category['type'] != 0) {
            return $this->response(1301);
        }

        // 模型不匹配 不允许发布文章
        if ($category['table_name'] != 'book') {
            return $this->response(1302);
        }

        $logic = new BookLogic();
        $ret = $logic->modify($data);

        if (is_numeric($ret)) {
            return $this->response(200);
        } else {
            return $this->response(201,$ret);
        }
    }

    public function remove()
    {
        $id = Request::instance()->param('id');
        $result = BookModel::remove($id);
        if ($result !== false) {
            return $this->response(200);
        } else {
            return $this->response(100);
        }
    }

}
