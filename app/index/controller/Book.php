<?php
namespace app\index\controller;

use think\Request;
use app\common\service\Book as BookLogic;
use app\index\controller\Base;

class Book extends Base
{

    protected $data = [];

    function _initialize()
    {
        $this->data = $this->getData();
    }

    protected function getData()
    {
        $logic = new BookLogic();
        return $logic->getInfo(Request::instance()->param('id'));
    }


    public function read()
    {
        if (!empty($this->data)) {
            // 数组键名转换为变量 并传值到模板
            foreach ($this->data as $k => $v) {
                $this->assign($k,$v);
            }
        } else {
            $this->error('您访问的页面不存在!');
        }

        // 更新访问次数


        // 模版 优先使用栏目独立模版配置
        return $this->fetch($this->template('book_content_tpl', $this->data['content_template']));
    }
}
