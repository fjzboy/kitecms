<?php
namespace app\index\controller;

use think\Request;
use app\common\service\Article as ArticleLogic;
use app\index\controller\Base;

class Article extends Base
{

    protected $data = [];

    function _initialize()
    {
        $this->data = $this->getData();
    }

    protected function getData()
    {
        $article_id = Request::instance()->param('id');
        $articleLogic = new ArticleLogic();
        $articleLogic->setPv($article_id);
        return $articleLogic->getInfo($article_id);
    }


    public function read()
    {
        if (!empty($this->data)) {
            // 数组键名转换为变量 并传值到模板
            foreach ($this->data as $k => $v) {
                $this->assign($k,$v);
            }
        } else {
            $this->error('您访问的页面不存在!');
        }

        // 模版 优先使用栏目独立模版配置
        return $this->fetch($this->template('article_content_tpl', $this->data['content_template']));
    }
}
