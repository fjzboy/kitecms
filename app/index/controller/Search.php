<?php
namespace app\index\controller;

use think\Request;
use app\index\controller\Base;
use app\common\service\Article as ArticleLogic;
use app\common\service\Book as BookLogic;

class Search extends Base
{
    public function index()
    {
        $params['keywords'] = Request::instance()->param('keywords');

        // 文章搜索
        $amap['a.title']  = ['like', '%'.$params['keywords'].'%'];
        $articleLogic = new ArticleLogic();
        $article_list = $articleLogic->getAll($amap, 'id desc', $this->config('article_limit'), $params);

        // 图书搜索
        $bmap['b.title']  = ['like', '%'.$params['keywords'].'%'];
        $bookLogic = new BookLogic();
        $book_list = $bookLogic->getAll($bmap, 'id desc', $this->config('book_limit'), $params);

        $this->assign('article_list', $article_list);
        $this->assign('book_list', $book_list);
        $this->assign('keywords', $params['keywords']);
        return $this->fetch($this->template('search_tpl'));
    }
}
