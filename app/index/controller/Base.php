<?php
namespace app\index\controller;

use think\Request;
use app\common\controller\Common;

class Base extends Common
{
    /** 
     * 当前使用主题
     *
     * @return string
     */
    public function theme()
    {
        $theme = $this->config('theme');
        if (empty($theme)) {
            $this->error('未配置模板主题');
        }

        return $theme;
    }

    /** 
     * 当前使用模版信息
     *
     * @params $default string 配置表模版
     * @params $custom string 栏目自定义配置模版
     */
    public function template($default, $custom = '')
    {
        $temp = $this->config($default);
        if (empty($temp) && empty($custom)) {
            $this->error('未配置模板页面');
        }

        $template = empty($custom) ? $temp : $custom;
        return $this->theme() . DS . $template;
    }
}
