<?php
namespace app\common\validate;
use think\Validate;

class UserValidate extends Validate
{
    protected $rule = [
        'username'  => 'require|max:64',
        'email'     => 'email',
        'phone'     => 'require|max:16',
    ];

    protected $message = [
        'username.require' => '账户必须',
        'username.max'     => '账户最多不能超过64个字符',
        'email'            => '邮箱格式错误',
        'phone'            => '电话号码最长16位',
    ];
    
    protected $scene = [
        'add'  => ['username', 'email', 'phone'],
        'edit' => ['email', 'phone'],
    ];
}