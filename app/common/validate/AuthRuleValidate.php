<?php
namespace app\common\validate;
use think\Validate;

class AuthRuleValidate extends Validate
{
    protected $rule = [
        'name'  =>  'require|max:64',
    ];

    protected $message = [
        'name.require' => '成员组名称必须',
        'name.max'     => '成员组名称最多不能超过64个字符',
    ];
    
    protected $scene = [
        'add'   =>  ['name'],
        'edit'  =>  ['name'],
    ];
}