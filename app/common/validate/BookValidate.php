<?php
namespace app\common\validate;

use think\Validate;

class BookValidate extends Validate
{
    protected $rule = [
        'title' => 'require|max:150',
    ];

    protected $message = [
        'title.require'  => '标题名称必须',
        'title.max'      => '标题名称最多不能超过150个字符',
    ];
    
    protected $scene = [
        'add'   =>  ['title'],
        'edit'  =>  ['title'],
    ];
}