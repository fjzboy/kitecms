<?php
namespace app\common\logic;

use think\Model;
use think\Loader;
use app\common\validate\BookValidate;
use app\common\model\Book as BookModel;

class Book extends Model
{
    /*
     * 创建信息
     *
     * @params $data array
     * @return mix
     */
    public function add($data)
    {
        $validate = Loader::validate('BookValidate');
        $result = $validate->scene('add')->check($data);

        if(!$result){
            return $validate->getError();
        }

        // 保存图片 转换通用斜杠符号
        if (isset($data['image'])){
            $data['image'] = str_replace("\\", "/", $data['image']);
        }
        $data['create_at'] = date('Y-m-d H:i:s',time());

        return BookModel::add($data);
    }

    /*
     * 更新信息
     *
     * @params $data array 数据
     * @return mix
    */
    public function modify($data)
    {
        $validate = Loader::validate('BookValidate');
        $result = $validate->scene('edit')->check($data);

        if(!$result){
            return $validate->getError();
        }

        // 枚举类型判空
        if (empty($data['recommend'])) {
            $data['recommend'] = 0;
        }
        if (empty($data['hot'])) {
            $data['hot'] = 0;
        }
        if (empty($data['is_section'])) {
            $data['is_section'] = 0;
        }
        if (empty($data['flag'])) {
            $data['flag'] = 0;
        }
        if (empty($data['status'])) {
            $data['status'] = 0;
        }

        // 替换图片格式
        if (!empty($data['image'])) {
            $data['image'] = str_replace("\\", "/", $data['image']);
        }
        $data['update_at'] = date('Y-m-d H:i:s',time());

        return BookModel::modify($data);
    }
}