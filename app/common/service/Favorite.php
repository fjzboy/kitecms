<?php
namespace app\common\service;

use app\common\model\Favorite as FavoriteModel;
use app\common\service\ServiceBase;

class Favorite extends ServiceBase
{
    /**
     * 收藏的文章列表
     *
     * @params $uid int 用户编号ID
     * @return array
     */
    public function getArticleList($uid, $limit)
    {
        $list = FavoriteModel::getArticleList($uid, $limit);
        $data['list'] = $this->formatArticle($list);
        $data['page'] = $list->render();

        return $data;
    }

    /**
     * 收藏的文章列表
     *
     * @params $uid int 用户编号ID
     * @return array
     */
    public function getBookList($uid, $limit)
    {
        $list = FavoriteModel::getBookList($uid, $limit);
        $data['list'] = $this->formatBook($list);
        $data['page'] = $list->render();

        return $data;
    }
}