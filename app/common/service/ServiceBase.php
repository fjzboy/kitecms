<?php
namespace app\common\service;

use think\Model;
use app\common\logic\Common;

class ServiceBase extends Common
{
    /**
     * 文章数据 格式化
     *
     * @params $data array 文章数据
     * @return array|boolean
     */
    public function formatArticle($data)
    {
        // 判断一维数组 还是多维数组
        $result = array();
        $flag   = 0;
        if (!empty($data)) {
            foreach($data as $v) {
                if (is_array($v)) {
                    array_push($result, $this->formatArticleRule($v));
                } else {
                    $flag = 1; //如果不是多维数组 给标识1
                }
            }
        }

        // 格式化一维数组
        if ($flag == 1) {
            $result = $this->formatArticleRule($data);
        }

        return $result;
    }

    /**
     * 消息数据 格式化
     *
     * @params $data array 消息数据
     * @return array|boolean
     */
    public function formatMessage($data)
    {
        // 判断一维数组 还是多维数组
        $result = array();
        $flag   = 0;
        if (!empty($data)) {
            foreach($data as $v) {
                if (is_array($v)) {
                    array_push($result, $this->formatMessageRule($v));
                } else {
                    $flag = 1; //如果不是多维数组 给标识1
                }
            }
        }

        // 格式化一维数组
        if ($flag == 1) {
            $result = $this->formatMessageRule($data);
        }

        return $result;
    }

    /**
     * 图书数据 格式化
     *
     * @params $data array 文章数据
     * @return array|boolean
     */
    public function formatBook($data)
    {
        // 判断一维数组 还是多维数组
        $result = array();
        $flag   = 0;
        if (!empty($data)) {
            foreach($data as $v) {
                if (is_array($v)) {
                    array_push($result, $this->formatBookRule($v));
                } else {
                    $flag = 1; //如果不是多维数组 给标识1
                }
            }
        }

        // 格式化一维数组
        if ($flag == 1) {
            $result = $this->formatBookRule($data);
        }

        return $result;
    }

    /**
     * 评论数据 格式化
     *
     * @params $data array 文章数据
     * @return array|boolean
     */
    public function formatComment($data)
    {
        // 判断一维数组 还是多维数组
        $result = array();
        $flag   = 0;
        if (!empty($data)) {
            foreach($data as $v) {
                if (is_array($v)) {
                    array_push($result, $this->formatCommentRule($v));
                } else {
                    $flag = 1; //如果不是多维数组 给标识1
                }
            }
        }

        // 格式化一维数组
        if ($flag == 1) {
            $result = $this->formatCommentRule($data);
        }

        return $result;
    }

    /**
     * 文章数据 格式化规则
     *
     * @params $data array 文章数据
     * @return array|boolean
     */
    public function formatArticleRule($data)
    {
        if (!empty($data)) {

            // 创建URL及其他数据
            $data['category_url']   = $this->categoryUrl($data['category_alias']);
            $data['article_url']    = $this->articleUrl($data['id']);
            $data['member_url']     = $this->memberUrl($data['uid']);
            $data['comment_url']    = $this->commentUrl($data['id']);
            
            // 统计评论数量
            $data['comments_total'] = \app\common\model\Comment::getTotal($data['id']);

            // 匹配文章所有图片
            $data['images'] = pickImage($data['content']);

            // 转换文章封面图片地址 如果没有自定义封面 匹配内容中第一张作为封面
            if (!empty($data['image'])) {
                $data['image'] = buildImageUrl($data['image']);
            } else {
                $data['image'] = !empty($data['images']) ? $data['images'][0] : '';
            }

            // 转换用户头像地址
            if (!empty($data['avatar'])) {
                 $data['avatar'] = buildImageUrl($data['avatar']);
            }
        }

        return $data;
    }

    /**
     * 消息内容数据 格式化
     *
     * @params $data array 消息详情数据
     * @return array|boolean
     */
    public function formatMessageRule($data)
    {
        if (!empty($data)) {
            $data['member_url'] = $this->memberUrl($data['from_uid']);
            $data['message_url'] = $this->messageUrl($data['id']);
            $data['type'] = $data['type'] == 1 ? '私信' : '系统消息';
            $data['status'] = $data['status'] == 1 ? '已阅' : '未阅';
        }
        return $data;
    }

    /**
     * 文章数据 格式化规则
     *
     * @params $data array 文章数据
     * @return array|boolean
     */
    public function formatBookRule($data)
    {
        if (!empty($data)) {

            // 创建URL及其他数据
            $data['category_url'] = $this->categoryUrl($data['category_alias']);
            $data['book_url'] = $this->bookUrl($data['id']);

            // 转换文章封面图片地址
            if (!empty($data['image'])) {
                $data['image'] = buildImageUrl($data['image']);
            }
        }

        return $data;
    }

    /**
     * 评论 格式化规则
     *
     * @params $data array 文章数据
     * @return array|boolean
     */
    public function formatCommentRule($data)
    {
        if (!empty($data)) {

            // 创建URL及其他数据
            $data['article_url'] = $this->articleUrl($data['article_id']);

            // 转换文章封面图片地址
            if (!empty($data['article_image'])) {
                $data['article_image'] = buildImageUrl($data['article_image']);
            }

            // 评论发布者头像
            if (!empty($data['from_avatar'])) {
                $data['from_avatar'] = buildImageUrl($data['from_avatar']);
            }
        }

        return $data;
    }

    /**
     * 生成文章链接
     *
     */
    public function articleUrl($article_id)
    {
        return \think\Url::build('index/article/read', ['id' => $article_id]);
    }

    /**
     * 生成分类链接
     *
     */
    public function categoryUrl($alias)
    {
        return \think\Url::build('index/category/read', ['alias' => $alias]);
    }

    /**
     * 生成TAG链接
     *
     */
    public function tagUrl($tag_id)
    {
        return \think\Url::build('index/tag/read', ['id' => $tag_id]);
    }

    /**
     * 生成会员主页链接
     *
     */
    public function memberUrl($uid)
    {
        return \think\Url::build('index/member/profile', ['uid' => $uid]);
    }

    /**
     * 生成私信详情链接
     *
     */
    public function messageUrl($id)
    {
        return \think\Url::build('index/member/readMessage', ['id' => $id]);
    }

    /**
     * 图书链接
     *
     */
    public function bookUrl($book_id)
    {
        return \think\Url::build('index/book/read', ['id' => $book_id]);
    }

    /**
     * 评论链接
     *
     */
    public function commentUrl($article_id)
    {
        return \think\Url::build('index/comment/read', ['id' => $article_id]);
    }

}