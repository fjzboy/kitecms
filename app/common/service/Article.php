<?php
namespace app\common\service;

use think\Request;
use app\common\model\Article as ArticleModel;
use app\common\model\Category as CategoryModel;
use app\common\model\Comment as CommentModel;
use app\common\service\Tag as TagLogic;
use app\common\service\ServiceBase;

class Article extends ServiceBase
{
    /**
     * 获取文章信息
     *
     * @params $article_id int 文章ID
     * @return array
     */
    public function getInfo($article_id)
    {
        $data = ArticleModel::getInfo($article_id);

        // 下一篇
        $next = ArticleModel::getNext($article_id);
        $data['next_url'] = !empty($next['id']) ? url('index/article/read', ['id' => $next['id']]) : '';
        $data['next_name'] = !empty($next['id']) ? $next['title'] : '';

        // 上一篇
        $prev = ArticleModel::getPrev($article_id);
        $data['prev_url'] = !empty($prev['id']) ? url('index/article/read', ['id' => $prev['id']]) : '';
        $data['prev_name'] = !empty($prev['id']) ? $prev['title'] : '';


        return $this->formatArticle($data);
    }

    /**
     * 获取指定推荐位列表(推荐 热门)
     *
     * @params $category_id array 分类ID
     * @params $recommend int 是否推荐 1
     * @params $hot int 是否热门 1
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public function getPosition($category_id, $recommend, $hot, $order, $limit)
    {
        // 查询规则 1 分类ID等于null 查询所有分类下文章
        //          2 分类ID等于self 查询当前栏目下文章
        //          3 分类ID等于栏目ID 查询指定分类,查询多个分类，ID用逗号隔开

        // 查询当前栏目下文章
        if ($category_id == 'self') {
            $alias = Request::instance()->param('alias');
            $map['category_id'] = CategoryModel::getID($alias);
        }

        // 查询多个栏目
        if ($category_id != 'null' && $category_id != 'self'){
            $map['category_id'] = array('in', $category_id);
        }

        // 是否推荐
        if ($recommend == 1) {
            $map['recommend'] = 1;
        }

        // 是否热门
        if ($hot == 1) {
            $map['hot'] = 1;
        }

        // 查询限制
        $map['a.status'] = 1; // 状态 0待审核, 1已审核
        $list = ArticleModel::getPosition($map, $order, $limit);
        return $this->formatArticle($list);
    }

    /**
     * 获取列表
     *
     * @params $$map array 查询条件
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public function getAll($map, $order, $limit, $params = [])
    {
        // 查询
        $map['a.status'] = 1; // 状态 0待审核, 1已审核
        $list = ArticleModel::getList($map, $order, $limit, $params);
        $data['list'] = $this->formatArticle($list);
        $data['page'] = $list->render();
        return $data;
    }

    /**
     * 获取指定栏目文章列表(分页)
     *
     * @params $category_id array 分类ID
     * @params $recommend int 是否推荐 1
     * @params $hot int 是否热门 1
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public function getList($category_id, $recommend, $hot, $order, $limit)
    {
        // 查询规则 1 分类ID等于null 查询所有分类下文章
        //          2 分类ID等于self 查询当前栏目下文章
        //          3 分类ID等于栏目ID 查询指定分类,查询多个分类，ID用逗号隔开

        // 查询当前栏目下文章
        if ($category_id == 'self') {
            $alias = Request::instance()->param('alias');
            $map['category_id'] = CategoryModel::getID($alias);
        }

        // 查询多个栏目
        if ($category_id != 'null' && $category_id != 'self'){
            $map['category_id'] = array('in', $category_id);
        }

        // 是否推荐
        if ($recommend == 1) {
            $map['recommend'] = 1;
        }

        // 是否热门
        if ($hot == 1) {
            $map['hot'] = 1;
        }

        // 查询
        $map['a.status'] = 1; // 状态 0待审核, 1已审核
        $list = ArticleModel::getList($map, $order, $limit);
        return $this->formatArticle($list);
    }
    
    /**
     * 获取文章分页
     *
     * @params $map array 查询条件
     * @params $page_size int 查询数量
     * @params $params array 分页附带参数
     * @return string
     */
    public function getPage($category_id, $recommend, $hot, $limit)
    {
        // 查询规则 1 分类ID等于null 查询所有分类下文章
        //          2 分类ID等于self 查询当前栏目下文章
        //          3 分类ID等于栏目ID 查询指定分类,查询多个分类，ID用逗号隔开

        // 查询当前栏目下文章
        if ($category_id == 'self') {
            $alias = Request::instance()->param('alias');
            $map['category_id'] = CategoryModel::getID($alias);
        }

        // 查询多个栏目
        if ($category_id != 'null' && $category_id != 'self'){
            $map['category_id'] = array('in', $category_id);
        }

        // 是否推荐
        if ($recommend == 1) {
            $map['recommend'] = 1;
        }

        // 是否热门
        if ($hot == 1) {
            $map['hot'] = 1;
        }

        // 查询
        $map['status'] = 1; //状态 1待审核, 2已审核
        return ArticleModel::getPage($map, $limit);
    }

    /**
     * 获取相关文章
     *
     * @params $article_id int 当前文章ID
     * @params $limit int 数量
     * @return array
     */
    public function getRelationArtcle($article_id, $limit)
    {
        // 查询规则
            // 1 文章ID为 NULL或者SELF 查询当前文章
            // 2 查询指定文章的相关列表 (目前根据TAG来判断相关)
        if ($article_id == 'self' || $article_id == 'null') {
            $article_id = Request::instance()->param('id');
        }

        $tagLogic = new TagLogic;
        $ids = $tagLogic->getTagRelation($article_id, $limit);
        if (empty($ids)) {
            return false;
        }

        // 查询文章列表
        $map['a.id'] = array('in', $ids);
        $list = ArticleModel::getList($map, 'a.id asc', $limit);

        return $this->formatArticle($list);
    }

    /**
     * 更新PV数
     *
     * @params $article_id int 文章ID
     * @return bool
     */
    public function setPv($article_id)
    {
        $pv = ArticleModel::getPv($article_id);

        // 更新PV
        return ArticleModel::setPv($article_id, $pv+1);
    }

    /**
     * 获取我的文章
     *
     * @params $uid int 用户UID
     * @params $limit int 数量
     * @return array
     */
    public function myArticle($uid, $limit)
    {
        $map = array(
            'a.uid' => $uid,
        );
        $list = ArticleModel::getList($map, 'a.id desc', $limit);
        $data['list'] = $this->formatArticle($list);
        $data['page'] = $list->render();

        return $data;
    }

}