<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Book extends Model
{
    /**
     * 获取图书信息
     *
     * @params $book_id int 图书ID
     * @return array
    */
    public static function getInfo($book_id)
    {
        return Db::name('book')
            ->alias('b')
            ->field('b.*, c.alias as category_alias, c.title as category_name, c.content_template, u.username')
            ->where('b.id', '=', $book_id)
            ->join('__CATEGORY__ c','b.category_id = c.id','LEFT')
            ->join('__USER__ u', 'b.uid = u.uid', 'LEFT')
            ->find();
    }

    /**
     * 图书列表(分页)
     *
     * @params $map array 查询条件
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
    */
    public static function getList($map, $order, $limit, $params = [])
    {
        return Db::name('book')
            ->alias('b')
            ->field('b.*, c.alias as category_alias, c.title as category_name, c.content_template, u.username')
            ->where($map)
            ->join('__CATEGORY__ c','b.category_id = c.id','LEFT')
            ->join('__USER__ u', 'b.uid = u.uid', 'LEFT')
            ->order($order)
            ->paginate($limit, false, ['query' => $params]);
    }

    /**
     * 图书列表(不分页)
     *
     * @params $map array 查询条件
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
    */
    public static function getAll($map, $order, $limit)
    {
        return Db::name('book')
            ->alias('b')
            ->field('b.*, c.alias as category_alias, c.title as category_name, c.content_template, u.username')
            ->where($map)
            ->join('__CATEGORY__ c','b.category_id = c.id','LEFT')
            ->join('__USER__ u', 'b.uid = u.uid', 'LEFT')
            ->order($order)
            ->limit($limit)
            ->select();
    }

    /**
     * 获取列表分页
     *
     * @params $map array 查询条件
     * @params $limit int 查询数量
     * @return array
    */
    public static function getPage($map, $limit)
    {
        $data = Db::name('book')
            ->where($map)
            ->paginate($limit);

        return $data->render();
    }

    /**
     * 获取最新章节列表
     *
     * @return array
    */
    public static function getNewChapterList()
    {
        return Db::name('book_chapter')
            ->alias('bc')
            ->field('bc.*, b.title as book_title, u.username')
            ->join('__BOOK__ b','b.id = bc.book_id','LEFT')
            ->join('__USER__ u', 'bc.uid = u.uid', 'LEFT')
            ->order('create_at desc')
            ->find();
    }

    /**
     * 获取章节信息
     *
     * @params $chapter_id int 图书ID
     * @return array
    */
    public static function getChapter($chapter_id)
    {
        return Db::name('book_chapter')
            ->alias('bc')
            ->field('bc.*, b.title as book_title, u.username')
            ->where('bc.id', '=', $chapter_id)
            ->join('__BOOK__ b','b.id = bc.book_id','LEFT')
            ->join('__USER__ u', 'bc.uid = u.uid', 'LEFT')
            ->find();
    }

    /**
     * 获取上一条信息
     *
     * @params $$chapter_id int 当前章节ID
     * @return array
    */
    public static function getPrev($chapter_id)
    {
        return Db::name('book_chapter')
            ->where('id', '<', $chapter_id)
            ->order('id desc')
            ->find();
    }

    /**
     * 获取下一条信息
     *
     * @params $chapter_id int 当前章节ID
     * @return array
    */
    public static function getNext($chapter_id)
    {
        return Db::name('book_chapter')
            ->where('id', '>', $chapter_id)
            ->order('id asc')
            ->find();
    }

    /**
     * 获取图书分卷列表
     *
     * @params $book_id int 图书ID
     * @return array
    */
    public static function getSectionList($book_id)
    {
        return Db::name('book_section')
            ->where('book_id', '=', $book_id)
            ->select();
    }

    /**
     * 获取图书分卷列表
     
     * @params $map array 条件数组
     * @params $order array|string 排序
     * @return array
    */
    public static function getChapterList($map, $order)
    {
        return Db::name('book_chapter')
            ->where($map)
            ->order($order)
            ->select();
    }

    /**
     * 创建
     *
     * @params $data array
     * @return mix
     */
    public static function add($data)
    {
        return Db::name('book')->insertGetId($data);
    }

    /**
     * 更新
     *
     * @params $data array
     * @return mix
     */
    public static function modify($data)
    {
        return Db::name('book')->update($data);
    }

    /*
     * 推荐
     *
     * @params $id
     * @return mix
     */
    public static function recommend($id, $value)
    {
        return Db::name('book')->where('id',$id)->setField('recommend', $value);
    }

    /**
     * 热门
     *
     * @params $id
     * @return mix
     */
    public static function hot($id ,$value)
    {
        return Db::name('book')->where('id',$id)->setField('hot', $value);
    }

    /**
     * 设置封面图片
     *
     * @params $image
     * @return mix
     */
    public static function setImage($id ,$value)
    {
        return Db::name('book')->where('id',$id)->setField('image', $value);
    }

    /**
     * 设置访问次数
     *
     * @params $image
     * @return mix
     */
    public static function setPv($id, $times)
    {
        return Db::name('book')->where('id',$article_id)->setField('pv', $times);
    }

    /**
     * 审核
     *
     * @params $id int 文章ID
     * @params $value int 0 待审 1通过
     * @return mix
     */
    public static function setStatus($id ,$value)
    {
        return Db::name('book')->where('id',$id)->setField('status', $value);
    }

    /**
     * 删除
     *
     * @params $id
     * @return mix
     */
    public static function remove($id)
    {
        return Db::name('book')->delete($id);
    }
}