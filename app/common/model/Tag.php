<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Tag extends Model
{
    /**
     * 根据文章ID获取TAG
     *
     * @params $article_id int 文章ID
     * @return array
     */
    public static function getTag($article_id)
    {
        return $list = Db::name('article_tag_access')
            ->alias('a')
            ->field('a.article_id, t.*')
            ->join('__ARTICLE_TAG__ t', 'a.tag_id = t.id', 'LEFT')
            ->where('article_id', '=', $article_id)
            ->select();
    }

    /**
     * 根据文章TagID获取相关文章ID集合
     *
     * @params $map   mix          查询条件
     * @params $limit int          数量
     * @return array 文章ID集合
     */
    public static function getTagRelation($map, $limit)
    {
        return $list = Db::name('article_tag_access')
            ->where($map)
            ->order('article_id desc')
            ->limit($limit)
            ->select();
    }

    /**
     * 获取TAG列表
     *
     * @params $map   mix          查询条件
     * @params $order string|array 排序
     * @params $limit int          限制条数
     * @return array
     */
    public static function getList($map, $order, $limit)
    {
        return $list = Db::name('article_tag')
            ->where($map)
            ->order($order)
            ->limit($limit)
            ->select();
    }

    /**
     * 查询TAG
     *
     * @params $tag_id int ID
     * @return array
     */
    public static function getInfo($tag_id)
    {
        return Db::name('article_tag')
            ->where('id', '=', $tag_id)
            ->find();
    }

    /**
     * 查询TAG信息
     *
     * @params $tagName string tag名字
     * @return array
     */
    public static function findTag($tagName)
    {
        return Db::name('article_tag')
            ->where('title', '=', $tagName)
            ->find();
    }

    /**
     * 增加TAG
     *
     * @params $tagName string tag名
     * @return array
     */
    public static function addTag($tagName)
    {
        $data = array(
            'title' => $tagName,
            'num'  => 1,
        );
        return Db::name('article_tag')->insertGetId($data);
    }

    /**
     * TAG与文章 绑定关系
     *
     * @params $tag_id     int tagID
     * @params $article_id int 文章ID
     * @return boolean
     */
    public static function bind($tag_id, $article_id)
    {
        $data = array(
            'tag_id'     => $tag_id,
            'article_id' => $article_id,
        );
        return Db::name('article_tag_access')->insert($data);
    }

    /**
     * 设置TAG引用数量
     *
     * @params $tag_id int ID
     * @params $num    int 引用数量
     * @return boolean
     */
    public static function setTagNum($tag_id, $num)
    {
        return Db::name('article_tag')
            ->where('id',$tag_id)
            ->setField('num', $num);
    }

    /**
     * 根据Tag id获取文章列表
     *
     * @params $map array 查询条件
     * @params $order array|string 排序规则
     * @params $limit int 数量
     * @return array|boolean
     */
    public static function getArticleList($map, $order, $limit)
    {
        return Db::name('article_tag_access')
            ->alias('t')
            ->field('a.*,t.tag_id,c.alias as category_alias, c.title as category_name, c.content_template, u.username, u.nickname, u.motto, u.avatar')
            ->where($map)
            ->join('__ARTICLE__ a','a.id = t.article_id','LEFT')
            ->join('__CATEGORY__ c','a.category_id = c.id','LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->order($order)
            ->paginate($limit);
    }

    /**
     * 根据Tag id获取文章列表
     *
     * @params $map array 查询条件
     * @params $limit int 数量
     * @return array|boolean
     */
    public static function getArticlePage($map, $limit)
    {
        $data = Db::name('article_tag_access')
            ->alias('t')
            ->field('t.article_id')
            ->where($map)
            ->join('__ARTICLE__ a','a.id = t.article_id','LEFT')
            ->paginate($limit);

        return $data->render();
    }

    /**
     * 接触TAG绑定关系
     *
     * @params $article_id int 文章ID
     * @params $tag_id int TAGID
     * @return boolean
     */
    public static function removeAccess($article_id)
    {
        return Db::name('article_tag_access')->where('article_id', $article_id)->delete();
    }
}