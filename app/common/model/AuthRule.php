<?php
namespace app\common\model;

use think\Model;
use think\Db;

class AuthRule extends Model
{
    /**
     * 获取用户组详情信息

     * @params $map array 查询条件
     * @return array|bool
     */
    public static function getInfo($map)
    {
        return Db::name('auth_rule')->where($map)->find();
    }
    
    /**
     * 获取权限规则列表

     * @params $map array 查询条件
     * @params $order array|string 排序规则
     * @return array
     */
    public static function getList($map = [], $order = 'reorder asc')
    {
        return Db::name('auth_rule')->where($map)->order($order)->select();
    }

    /**
     * 获取权限规则列表(Tree)

     * @params $map array 查询条件
     * @params $order array|string 排序规则
     * @return array
     */
    public static function getListTree($map = [], $order = 'reorder asc')
    {
        $list = Db::name('auth_rule')->where($map)->order($order)->select();
        return list_to_tree($list, $pk='id', $pid = 'parent_id', $child = 'child', $root = 0);
    }

    /**
     * 删除
     *
     * @params $id int 分类ID
     * @return int
     */
    public static function remove($id)
    {
        return Db::name('auth_rule')->delete($id);
    }

    /**
     * 排序
     *
     * @params $id int 分类ID
     * @params $order int 排序数值
     * @return int
     */
    public static function reorder($id, $order)
    {
        return Db::name('auth_rule')->where('id', $id)->setField('reorder', $order);
    }

    /**
     * 查询子规则
     *
     * @params $parent_id int 分类ID
     * @return array
     */
    public static function getChild($parent_id)
    {
        return Db::name('auth_rule')->where('parent_id', $parent_id)->select();
    }

    /**
     * 创建 
     *
     * @params $data array 创建数据
     * @return mix
     */
    public static function add($data)
    {
        return Db::name('auth_rule')->insertGetId($data);
    }

    /**
     * 更新
     *
     * @params $data array 需要更新的数据
     * @return mix
    */
    public static function edit($data)
    {
        return Db::name('auth_rule')->update($data);
    }
}