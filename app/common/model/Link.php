<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Link extends Model
{
    public static function getList($map = array())
    {
        return Db::name('link')->where($map)->order('id desc')->paginate(15);
    }

    public static function getInfo($id)
    {
        return Db::name('link')->where('id', $id)->find();
    }

    public static function add($data)
    {
        return Db::name('link')->insertGetId($data);
    }

    public static function edit($data)
    {
        return Db::name('link')->update($data);
    }
    
    public static function remove($id)
    {
        return Db::name('link')->delete($id);
    }

}