<?php
namespace app\common\taglib;

use think\template\TagLib;

class Cms extends TagLib{
    /**
     * 定义全局标签列表
     *
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'config' => ['attr' => 'key', 'close' => 0], //闭合标签，默认为不闭合
        'link' => ['attr' => 'name', 'close' => 1],//链接
        'slider' => ['attr' => 'name', 'close' => 1],//轮播图
        'domain' => ['attr' => '', 'close' => 0], //网站域名
        'name' => ['attr' => '', 'close' => 0], //网站名称
        'title' => ['attr' => '', 'close' => 0], //网站标题
        'keywords' => ['attr' => '', 'close' => 0], //网站关键词
        'description' => ['attr' => '', 'close' => 0], //网站描述
        'thumb' => ['attr' => 'file,width,height', 'close' => 0], //生成缩略图标签
        'member' => ['attr' => 'name,uid', 'close' => 1],//用户信息
        'menu' => ['attr' => 'name,class', 'close' => 1],//后台菜单
        'ueditor'    => ['attr' => 'name', 'close' => 1],   //百度编辑器
        'upload' => ['attr' => 'name,image,id,model', 'close' => 1],//上传图片组件
    ];

    /**
     * 获取配置值
     *
     */
    public function tagConfig($tag)
    {
        $key = $tag['key'];
        $parse = '<?php ';
        $parse .= 'echo app\common\model\Config::getValue("' . $key . '");';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 友情链接列表
     *
     */
    public function tagLink($tag, $content)
    {
        $name   = $tag['name'];
        $parse  = '<?php ';
        $parse .= '$__LIST__ = app\common\model\Link::getList();';
        $parse .= ' ?>';
        $parse .= '{volist name="__LIST__" id="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

    /**
     * 轮播图
     *
     */
    public function tagSlider($tag, $content)
    {
        $name   = $tag['name'];
        $parse  = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\logic\Slider(); ';
        $parse .= '$__LIST__ = $__MODEL__->getDisplay();';
        $parse .= ' ?>';
        $parse .= '{volist name="__LIST__" id="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

    /**
     * 网站域名
     *
     */
    public function tagDomain($tag)
    {
        $parse = '<?php ';
        $parse .= 'echo app\common\model\Config::getValue("domain");';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 网站名称
     *
     */
    public function tagName($tag)
    {
        $parse = '<?php ';
        $parse .= 'echo app\common\model\Config::getValue("name");';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 网站标题
     *
     */
    public function tagTitle($tag)
    {
        $parse = '<?php ';
        $parse .= 'echo app\common\model\Config::getValue("title");';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 网站关键词
     *
     */
    public function tagKeywords($tag)
    {
        $parse = '<?php ';
        $parse .= 'echo app\common\model\Config::getValue("keywords");';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 网站描述
     *
     */
    public function tagDescription($tag)
    {
        $parse = '<?php ';
        $parse .= 'echo app\common\model\Config::getValue("description");';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 生成缩略图
     *
     */
    public function tagThumb($tag, $content)
    {
        $file   = $tag['file'];
        $width   = empty($tag['width']) ? '150' : $tag['width'];
        $height   = empty($tag['height']) ? '150' : $tag['height'];
        $parse  = '<?php ';
        $parse .= 'echo app\common\service\Image::instance()->thumb(' . $file . ',' . $width . ',' . $height . ');';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 登陆用户信息
     *
     */
    public function tagMember($tag, $content)
    {
        $name   = $tag['name'];
        $uid    = empty($tag['uid']) ? 'null' : $tag['uid'];
        $parse  = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\logic\User(); ';
        $parse .= '$' . $name . ' = $__MODEL__->getUserInfo("' . $uid . '");';
        $parse .= ' ?>';
        $parse .= '{notempty name="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/notempty}';
        return $parse;
    }

    /**
     * 生成后台菜单
     *
     */
    public function tagMenu($tag, $content)
    {
        $name   = $tag['name'];
        $class   = empty($tag['class']) ? 'active' : $tag['class'];
        $parse  = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\logic\Menu(); ';
        $parse .= '$__LIST__ = $__MODEL__->createMenu("' . $class . '");';
        $parse .= ' ?>';
        $parse .= '{volist name="__LIST__" id="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

    /**
     * 编辑器调用标签
     *
     * 编辑器调用方法{cms:ueditor name="content" /}内容{/cms:ueditor}
     */
    public function tagUeditor($tag, $content)
    {
            $parse = '<textarea name="' . $tag['name'] . '"id="ueditor">' . $content . '</textarea>';
            $parse.='<script type="text/javascript" src="__STATIC__/static/ueditor/ueditor.config.js"></script>';
            $parse.='<script type="text/javascript" src="__STATIC__/static/ueditor/ueditor.all.min.js"></script>';
            $parse.='<script type="text/javascript">';
            $parse.='//实例化编辑器
                var ue = UE.getEditor("ueditor", {
                    initialFrameWidth: \'100%\',
                    initialFrameHeight:300,
                    scaleEnabled:true,
                });';
            $parse.='</script>';
            return $parse;
    }
    
}
