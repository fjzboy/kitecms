<?php
namespace app\common\taglib;

use think\template\TagLib;

class Comment extends TagLib{
    /**
     * 定义全局标签列表
     *
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'list'   => ['attr' => 'name,article_id,order,limit', 'close' => 1], //评论列表
    ];

    /**
     * 定义标签列表
     *
     */
    public function tagList($tag, $content)
    {
        $article_id = empty($tag['article_id']) ? 'null' : $tag['article_id'];
        $order      = empty($tag['order']) ? 'id desc' : $tag['order'];
        $limit      = empty($tag['limit']) ? 10 : $tag['limit'];
        $name       = $tag['name'];
        $parse = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\service\Comment(); ';
        $parse .= '$__LIST__ = $__MODEL__->getAll("' . $article_id . '","' . $order . '",' . $limit . ');';
        $parse .= ' ?>';
        $parse .= '{volist name="__LIST__" id="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

}